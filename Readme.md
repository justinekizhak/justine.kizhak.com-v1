<a name="top"></a>
[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org)
<a href="https://www.instagram.com/justinekizhak"><img src="https://i.imgur.com/G9YJUZI.png" alt="Instagram" align="right"></a>
<a href="https://twitter.com/justinekizhak"><img src="http://i.imgur.com/tXSoThF.png" alt="Twitter" align="right"></a>
<a href="https://www.facebook.com/justinekizhak"><img src="http://i.imgur.com/P3YfQoD.png" alt="Facebook" align="right"></a>
<br>

- - -

<!-- {Put your badges here} -->

- - -

# [ justine.kizhak.com ](https://justine.kizhak.com)

- - -

## Table of contents

- [Introduction](#introduction)
- [Features](#features)
- [Getting Started](#getting-started)
- [Full Documentation](#full-documentation)
- [License](#license)

## Introduction

**[Back to top](#table-of-contents)**

## Features

- Uses docker to develop locally
- The docker container stores the build files in the `justine.kizhak.com/_site` as well as in `public` directory
- Once pushed to Gitlab the files in `public` are copied to the deployment container and hosted there
- No more build problems. If the build worked locally then it works once uploaded.

**[Back to top](#table-of-contents)**

## Getting Started

- `docker-compose up` to develop locally

**[Back to top](#table-of-contents)**

## Full Documentation

To make this work for you
  - Start from scratch. Its easier that way.
  - Visit [jekyll-docker] They explain how it works.
  - To use a template
    - Delete all the files you created in the "mysite" directory (if you have been copy pasting the commands)
    - Paste the new files of your template
    - Thats it. Now `docker-compose up` should work for local development.

### For the people who are using Gitlab for deployment

Gitlab needs a folder called `public` to host your pages.
So you need to create a folder called `public` in your root. Then you need to update your`docker-compose.yml` file
which you created by following the instructions from [jekyll-docker].
``` yaml
version: "3"
services:
  site:
    command: jekyll serve
    image: jekyll/jekyll:latest
    volumes:
      - $PWD/mysite:/srv/jekyll
      - $PWD/vendor/bundle:/usr/local/bundle
      - $PWD/public:/srv/jekyll/_site
    ports:
      - 4000:4000
      - 35729:35729
      - 3000:3000
      -   80:4000
```

and your `.gitlab-ci.yml` file will be
```
image: ruby:2.3

pages:
  stage: deploy
  script:
  - ls # you need atleast on job. So a dummy job.
  artifacts:
    paths:
    - public
  only:
  - master
```

Thats it.

[jekyll-docker]: https://github.com/envygeeks/jekyll-docker/blob/master/README.md#docker-compose

For full documentation [read the docs]()

Visit [website]().

Read [CHANGELOG], [CODE OF CONDUCT], [CONTRIBUTING] guide.

[CHANGELOG]: CHANGELOG.md
[CODE OF CONDUCT]: CODE_OF_CONDUCT.md
[CONTRIBUTING]: CONTRIBUTING.md

## License

**[Back to top](#table-of-contents)**

- - -

[![forthebadge](https://forthebadge.com/images/badges/powered-by-jeffs-keyboard.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/certified-steve-bruhle.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/certified-elijah-wood.svg)](https://forthebadge.com)

- - -
